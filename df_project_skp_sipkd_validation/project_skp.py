from openerp.osv import fields, osv
from mx import DateTime



class project_skp(osv.Model):
    _inherit = 'project.skp'
    
    def _get_kode_rekening_full(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
            result =''
            try :
                if data.company_id :
                    if data.code_program : 
                        result = data.company_id.kode_sipkd +'.'+data.code_program
                    if data.code_program and data.code_kegiatan: 
                        result = data.company_id.kode_sipkd +'.'+data.code_program+'.'+ data.code_kegiatan
            except:
                result = data.code or '00-INCORRECT-00'
            
            res[data.id] = result
        return res
    def _search_kode_rekening_full(self, cr, uid, obj, name, args, context=None):
        ids = set()
        
        #print "args : ",args
        
        for cond in args:
            filter_condition= cond[1]
            filter_value = cond[2]
            
            domain = filter_value,
            cr.execute("""select skp.id
                       from project_skp skp
                       join res_company c on c.id = skp.company_id
                       where %s = 
                       CASE WHEN target_category_id='program' THEN c.kode_sipkd||'.'||skp.code_program
                            WHEN target_category_id='kegiatan' THEN  c.kode_sipkd||'.'||skp.code_program||'.'||skp.code_kegiatan
                            ELSE ''
                       END  
                       and skp.active
                  group by skp.id
                        """ ,domain)
                
            res_ids = set(id[0] for id in cr.fetchall())
            ids = ids and (ids & res_ids) or res_ids
        if ids:
            return [('id', 'in', tuple(ids))]
        return [('id', '=', '0')]
    _columns = {
        'rekening_code' :fields.function(_get_kode_rekening_full, method=True, string='Kode Rekening Full', type='char', readonly=True,store=False, fnct_search=_search_kode_rekening_full),
        'old_target_biaya'     : fields.float('Target Biaya Yang Diinput Awal' ,readonly=True),   
        'old_realisasi_biaya': fields.float( 'Realisasi Biaya Yang Diinput Awal',readonly=True),
        'target_skp_sipkd_notsame'           : fields.boolean('Target SKP Dan SIPD Tidak Sama',readonly=True),
        'realisasi_skp_sipkd_notsame'           : fields.boolean('Realisasi SKP Dan SIPD Tidak Sama',readonly=True),
        'sipkd_sync_date'     : fields.date('Tanggal Sinkronisasi Dengan SIPKD',readonly=True),
        'is_sipkd_sync'       : fields.boolean('Status Sinkronisasi SIPKD'),
        'sipkd_sync_state'       : fields.selection([     ('not_synced', 'Tidak Ada Data Sinkronisasi'), 
                                                         ('synced', 'Synced'),
                                  ],'Status Realisasi Biaya SIPKD',)
    }
    #_defaults ={
        #'is_sipkd_sync':False,
        #'sipkd_sync_state':'not_synced'
    #}
    def manual_syncronize_sipkd_integration(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.skp.sipkd.extraction')
        
        for task_id in ids:
            data_extraction_pool.sync_and_update_with_project_skp_by_task_id(cr,uid,task_id,context=None)
        
        return True
    def manual_syncronize_sipkd_integration_zero_points(self, cr, uid, ids,context=None):
        
        now=DateTime.today();
        for task_obj in self.browse(cr,uid,ids,context=None):
            task =  {}
            task.update({
                                    #'target_biaya':task_obj.old_target_biaya,
                                    'realisasi_biaya':0,
                                    'target_skp_sipkd_notsame':True,
                                    'realisasi_skp_sipkd_notsame':True,
                                    'sipkd_sync_date':now,
                                    'old_target_biaya':task_obj.target_biaya,
                                    'old_realisasi_biaya':task_obj.realisasi_biaya,
                                    'is_sipkd_sync':True,
                                    'sipkd_sync_state':'not_synced'
                        })
            self.write(cr,uid,[task_obj.id],task,context=None)
           
        
        return True  
    
    def manual_roolback_sipkd_integration(self, cr, uid, ids,context=None):
        
        
        for task_obj in self.browse(cr,uid,ids,context=None):
            task =  {}
            task.update({
                                    'target_biaya':task_obj.old_target_biaya,
                                    'realisasi_biaya':task_obj.old_realisasi_biaya,
                                    'target_skp_sipkd_notsame':False,
                                    'realisasi_skp_sipkd_notsame':False,
                                    'sipkd_sync_date':None,
                                    'old_target_biaya':None,
                                    'old_realisasi_biaya':None,
                                    'is_sipkd_sync':False,
                                    'sipkd_sync_state':'not_synced'
                        })
            self.write(cr,uid,[task_obj.id],task,context=None)
           
        
        return True  
    
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        #get var lock action done ?
        is_lock=False;
        try :
            lock_absen_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'lck_sipk'), ('active', '=', True), ('type', '=', 'lain_lain')], context=None)
            is_lock_int  = self.pool.get('config.skp').browse(cr, uid, lock_absen_ids)[0].config_value_int
            if is_lock_int == 1:
                is_lock=True;
            else :
                is_lock=False;
        except :
            is_lock=False;

        if is_lock : 
            duplicate_ids = self.read(cr, uid, ids, ['is_sipkd_sync','sipkd_sync_date','target_type_id','employee_job_type'], context=context)
            print "Filter only sync data"
            task_ids=[]
            for record in duplicate_ids:
                if record['target_type_id'] == 'dpa_opd_biro' :
                    if record['is_sipkd_sync'] :
                        task_ids.append(record['id'])
                    else :
                        continue;
                else :
                    task_ids.append(record['id'])
                    
            super(project_skp, self).action_done(cr, uid, task_ids, context=context)
        else :
            super(project_skp, self).action_done(cr, uid, ids, context=context)
            
        return True
    
      
project_skp()