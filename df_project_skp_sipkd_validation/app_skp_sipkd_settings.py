from openerp.osv import fields, osv
from mx import DateTime

class app_skp_sipkd_settings(osv.osv_memory):
    _name = 'app.skp.sipkd.settings'
    _description = 'Konfigurasi Aplikasi SKP Dan SIPKD'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP SIPKD', size=64, readonly=True),
            'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'target_period_year'     : fields.char('Periode Tahun', size=4, ),
    }
    def action_validate_to_project_skp_format_scheduler(self, cr, uid, context=None):

        data_input_pool = self.pool.get('project.skp.sipkd.data.integration')
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year = last_month.strftime('%Y')
        target_period_month = last_month.strftime('%m')
        date_now = now.strftime('%d')
        print "year : ", target_period_year
        print "month :", target_period_month
        print "sipkd date now : ",  date_now
        if date_now:
            print " exec now..."
            data_ids        = data_input_pool.search(cr, uid, [#('state', 'not in', ('synced','done')),
                                                   ('periode_tahun', '=', target_period_year),
                                                   ('periode_bulan', '=', target_period_month),
                                                   ('state','=','sipkd')
                                                   ],
                                        context=None,limit=100)
            if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                    data_input_pool.sync_with_project_skp(cr,uid,data_ids,context=None)
        
        return {}

    def action_revalidate_to_project_skp_format_scheduler(self, cr, uid, context=None):

        data_input_pool = self.pool.get('project.skp.sipkd.data.integration')
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year = last_month.strftime('%Y')
        target_period_month = last_month.strftime('%m')
        date_now = now.strftime('%d')
        print "re exec failed task : "
        # print "exec date v  date now : ", exec_date, " v ", date_now
        if date_now in [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]:
            print " exec now..."
            data_ids = data_input_pool.search(cr, uid, [#('state', 'not in', ('synced', 'done')),
                                                        ('periode_tahun', '=', target_period_year),
                                                        ('periode_bulan', '=', target_period_month),
                                                         ('state','=','failed')
                                                        ],
                                              context=None, limit=1000)
            if data_ids:
                print "data yang akan sinkron : ", len(data_ids)
                data_input_pool.sync_with_project_skp(cr, uid, data_ids, context=None)

        return {}


    def action_replace_data_project_skp_with_sipkd(self, cr, uid,  context=None):
        
        data_extraction_pool = self.pool.get('project.skp.sipkd.extraction')
        now = DateTime.today();
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year = last_month.strftime('%Y')
        target_period_month = last_month.strftime('%m')
        date_now = now.strftime('%d')
        #print "year : ", target_period_year
        #print "month :", target_period_month
        #for param_obj in self.browse(cr, uid, ids, context=context):
        if target_period_year:
                print "exec replace"
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'new'),
                                               ('target_period_year', '=', target_period_year),
                                               ('target_period_month', '=', target_period_month)],
                                    context=None)
                if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                data_extraction_pool.sync_and_update_with_project_skp(cr,uid,data_ids,context=None)
        return True
    def action_rollback_project_skp_with_sipkd(self, cr, uid, ids, context={}):
        
        data_extraction_pool = self.pool.get('project.skp.sipkd.extraction')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'synced'),
                                               ('target_period_year', '=', param_obj.target_period_year), 
                                               ('target_period_month', '=', param_obj.target_period_month)], 
                                    context=None)
                if data_ids:
                    print "data yang akan dirollback sinkron : ",len(data_ids)
                data_extraction_pool.rollback_sync_with_project_skp(cr,uid,data_ids,context=None)
        return True
app_skp_sipkd_settings()