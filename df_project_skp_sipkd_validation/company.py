# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv



class res_company(osv.Model):
    _inherit = 'res.company'
    
    def _get_kode_opd_sipkd(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data in self.browse(cr, uid, ids, context=context):
            try :
                result = data.kode_fungsi+'.'+data.kode_urusan+'.'+data.kode_opd
            except:
                result = data.code or '00'
            
            res[data.id] = result
        return res
    _columns = {
        'kode_fungsi' :fields.char('Kode Fungsi',size=1,required=True),
        'kode_urusan' :fields.char('Kode Urusan',size=2,required=True),
        'kode_opd' :fields.char('Kode OPD',size=2,required=True),
        'kode_sipkd' :fields.function(_get_kode_opd_sipkd, method=True, string='Kode OPD SIPKD', type='char', readonly=True,store=True),
    }
res_company()